package com.kimentii.driveassistant.views

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.kimentii.driveassistant.video.VideoReader
import org.opencv.core.Mat

class DrawingView : SurfaceView, SurfaceHolder.Callback {

    private lateinit var drawingThread: DrawingThread
    private lateinit var videoReader: VideoReader
    private lateinit var videoPath: String

    private var onFrameReceived: ((Mat) -> Mat)? = null
    private var onProgressUpdatedListener: ((Int) -> Unit)? = null

    private val mainThreadHandler = Handler(Looper.getMainLooper())

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    init {
        holder.addCallback(this)
    }

    fun setVideoPath(path: String) {
        videoPath = path
    }

    fun setOnFrameReceived(listener: (Mat) -> Mat) {
        onFrameReceived = listener
    }

    fun setOnProgressUpdatedListener(listener: (Int) -> Unit) {
        onProgressUpdatedListener = listener
    }

    override fun surfaceCreated(holder: SurfaceHolder) {}

    // In our case this method is called once when surface is created
    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
        drawingThread = DrawingThread(holder, width, height)
        drawingThread.start()

        videoReader = VideoReader(videoPath) {
            if (onFrameReceived == null) {
                drawingThread.draw(it)
            } else {
                drawingThread.draw(onFrameReceived!!.invoke(it))
            }
        }
        videoReader.setOnProgressUpdatedListener {
            mainThreadHandler.run {
                onProgressUpdatedListener?.invoke(it)
            }
        }
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        drawingThread.stopDrawing()
        videoReader.stopReading()
    }
}

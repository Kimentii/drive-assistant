package com.kimentii.driveassistant.views

import android.graphics.Bitmap
import android.view.SurfaceHolder
import org.opencv.android.Utils
import org.opencv.core.Mat
import java.util.concurrent.atomic.AtomicBoolean

class DrawingThread(
    private val surfaceHolder: SurfaceHolder,
    private val width: Int,
    private val height: Int
) : Thread() {

    @Volatile
    private var frameToDraw: Mat? = null
    private val isDrawing: AtomicBoolean = AtomicBoolean(true)

    fun draw(mat: Mat) {
        frameToDraw = mat
    }

    fun stopDrawing() {
        isDrawing.set(false)
    }

    override fun run() {
        while (isDrawing.get()) {
            val currentFrame = frameToDraw
            if (currentFrame != null) {
                val canvas = surfaceHolder.lockCanvas()
                if (canvas != null) {
                    val frameAsBitmap =
                        Bitmap.createBitmap(currentFrame.width(), currentFrame.height(), Bitmap.Config.ARGB_8888)
                    Utils.matToBitmap(currentFrame, frameAsBitmap)

                    val leftIndent = (width - currentFrame.width()).toFloat() / 2
                    val topIndent = (height - currentFrame.height()).toFloat() / 2
                    canvas.drawBitmap(frameAsBitmap, leftIndent, topIndent, null)

                    surfaceHolder.unlockCanvasAndPost(canvas)
                }
            }
        }
    }
}

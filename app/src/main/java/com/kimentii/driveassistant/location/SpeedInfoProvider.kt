package com.kimentii.driveassistant.location

import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle

class SpeedInfoProvider(
    private val locationManager: LocationManager,
    private val onSpeedChanged: (Float) -> Unit
) {

    companion object {
        private const val MINIMAL_UPDATE_TIME_IN_MS = 1000L
    }

    private val locationListener = object : LocationListener {

        private var lastSpeedValue: Float = 0f

        override fun onLocationChanged(location: Location) {
            if (location.hasSpeed() && lastSpeedValue != location.speed) {
                lastSpeedValue = location.speed
                onSpeedChanged(lastSpeedValue)
            }
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

        override fun onProviderEnabled(provider: String) {}

        override fun onProviderDisabled(provider: String) {}
    }

    init {
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            MINIMAL_UPDATE_TIME_IN_MS,
            0f,
            locationListener
        )
        locationManager.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            MINIMAL_UPDATE_TIME_IN_MS,
            0f,
            locationListener
        )
    }

    fun stopUpdating() {
        locationManager.removeUpdates(locationListener)
    }
}

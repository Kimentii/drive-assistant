package com.kimentii.driveassistant.tools

import android.content.Context
import android.util.Log
import com.kimentii.driveassistant.R
import com.kimentii.driveassistant.tools.CascadeClassifierType.PROHIBITORY_SIGNS_CLASSIFIER
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import java.io.FileOutputStream

object CascadeClassifierLoader {

    private const val TAG = "MyTag"
    private const val BUFFER_SIZE = 4096

    fun load(context: Context, classifierType: CascadeClassifierType): CascadeClassifier {
        val resId = when (classifierType) {
            PROHIBITORY_SIGNS_CLASSIFIER -> R.raw.speed_limit_sign
        }
        val inputStream = context.resources.openRawResource(resId)
        val classifierFileDir = context.getDir("classifiers", Context.MODE_PRIVATE)
        val classifierFile = File(classifierFileDir, "classifier.xml")
        val outputStream = FileOutputStream(classifierFile)

        val buffer = ByteArray(BUFFER_SIZE)
        var bytesRead: Int
        while (true) {
            bytesRead = inputStream.read(buffer)
            if (bytesRead == -1) {
                break
            }
            outputStream.write(buffer, 0, bytesRead)
        }

        inputStream.close()
        outputStream.close()

        val cascadeClassifier = CascadeClassifier(classifierFile.absolutePath)
        if (cascadeClassifier.empty()) {
            Log.e(TAG, "Failed to load cascade classifier")
        } else {
            Log.i(TAG, "Loaded cascade classifier from " + classifierFile.absolutePath)
        }

        classifierFileDir.delete()

        return cascadeClassifier
    }
}

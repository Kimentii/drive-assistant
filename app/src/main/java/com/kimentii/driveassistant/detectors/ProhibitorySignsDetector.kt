package com.kimentii.driveassistant.detectors

import android.content.Context
import com.kimentii.driveassistant.tools.CascadeClassifierLoader
import com.kimentii.driveassistant.tools.CascadeClassifierType.PROHIBITORY_SIGNS_CLASSIFIER
import org.opencv.core.Mat
import org.opencv.core.MatOfRect
import org.opencv.core.Size
import org.opencv.objdetect.CascadeClassifier

class ProhibitorySignsDetector(context: Context) : Detector {

    private val cascadeClassifier: CascadeClassifier

    init {
        cascadeClassifier = CascadeClassifierLoader.load(context, PROHIBITORY_SIGNS_CLASSIFIER)
    }

    override fun detect(image: Mat): MatOfRect {
        val matOfRect = MatOfRect()

        cascadeClassifier.detectMultiScale(
            image,
            matOfRect,
            1.2,
            2,
            0,
            Size(20.0, 20.0),
            Size(100.0, 100.0)
        )

        return matOfRect
    }
}

package com.kimentii.driveassistant.detectors

import org.opencv.core.Mat
import org.opencv.core.MatOfRect

interface Detector {

    fun detect(image: Mat): MatOfRect
}

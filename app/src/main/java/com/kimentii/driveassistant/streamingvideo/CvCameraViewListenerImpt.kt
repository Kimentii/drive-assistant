package com.kimentii.driveassistant.streamingvideo

import android.content.Context
import android.content.res.Resources
import android.location.LocationManager
import com.kimentii.driveassistant.R
import com.kimentii.driveassistant.classifier.Classifier
import com.kimentii.driveassistant.classifier.Sign
import com.kimentii.driveassistant.detectors.ProhibitorySignsDetector
import com.kimentii.driveassistant.location.SpeedInfoProvider
import com.kimentii.driveassistant.texttospeech.TextPronouncer
import com.kimentii.driveassistant.texttospeech.TextPronouncer.QueueMode
import org.opencv.android.CameraBridgeViewBase
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc
import java.io.File

class CvCameraViewListenerImpl(context: Context) : CameraBridgeViewBase.CvCameraViewListener2 {

    private val prohibitorySignsDetector = ProhibitorySignsDetector(context)
    private val speedInfoProvider: SpeedInfoProvider
    private val textPronouncer: TextPronouncer
    private val resources: Resources
    private val classifier: Classifier

    private var currentSpeedKmH: Float = 0f
    private var previousDetectedSigns = listOf<Sign>()

    init {
        resources = context.resources

        classifier = Classifier(File(context.filesDir, "bufferImage.jpg"))

        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        speedInfoProvider = SpeedInfoProvider(locationManager) { currentSpeedKmH = it * 3.6f }

        textPronouncer = TextPronouncer(context)
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat {
        val grayImage = inputFrame.gray()

        val speedLimitSigns = prohibitorySignsDetector.detect(inputFrame.gray()).toList()

        val rgbFrame = inputFrame.rgba()

        speedLimitSigns.forEach {
            rgbFrame.drawRectangle(it, Scalar(0.0, 255.0, 0.0, 255.0))
        }

        val signs = speedLimitSigns.map {
            val imageWithSign = grayImage.submat(it)
            classifier.classify(imageWithSign)
        }
            .filter { it !is Sign.Unknown }

        signs.forEach { sign ->
            Imgproc.putText(
                rgbFrame,
                sign::class.java.simpleName,
                Point(0.0, 50.0),
                Imgproc.FONT_HERSHEY_SIMPLEX,
                1.5,
                Scalar(255.0, 255.0, 255.0, 255.0)
            )

        }

        if (signs != previousDetectedSigns) {
            signs.forEach { sign ->
                if (sign !is Sign.Unknown) {
                    val textToSay = when (sign) {
                        is Sign.SpeedLimit -> sign.name + sign.limitKmH
                        else -> sign.name
                    }
                    textPronouncer.speak(textToSay, QueueMode.FLUSH)
                }
                if (sign is Sign.SpeedLimit) {
                    if (currentSpeedKmH > sign.limitKmH) {
                        textPronouncer.speak(resources.getString(R.string.alert_you_exceed_speed), QueueMode.ADD)
                    }
                }
            }
        }
        previousDetectedSigns = signs

        Imgproc.putText(
            rgbFrame,
            currentSpeedKmH.toString(),
            Point(0.0, 100.0),
            Imgproc.FONT_HERSHEY_SIMPLEX,
            1.5,
            Scalar(255.0, 255.0, 255.0, 255.0)
        )
        return rgbFrame
    }

    private fun Mat.drawRectangle(rectangle: Rect, color: Scalar) {
        Imgproc.rectangle(this, rectangle, color)
    }

    override fun onCameraViewStarted(width: Int, height: Int) {}

    override fun onCameraViewStopped() {
        speedInfoProvider.stopUpdating()
        textPronouncer.shutdown()
    }
}

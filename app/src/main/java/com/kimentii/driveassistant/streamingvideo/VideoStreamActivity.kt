package com.kimentii.driveassistant.streamingvideo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.kimentii.driveassistant.R
import org.opencv.android.*

class VideoStreamActivity : AppCompatActivity() {

    companion object {
        init {
            System.loadLibrary("caffe")
            System.loadLibrary("caffe_jni")
        }

        private const val TAG = "MyTag"
    }

    private lateinit var cameraView: CameraBridgeViewBase
    private val opencvLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            if (status == LoaderCallbackInterface.SUCCESS) {
                cameraView.setCameraIndex(JavaCamera2View.CAMERA_ID_BACK)
                cameraView.setCvCameraViewListener(
                    CvCameraViewListenerImpl(
                        applicationContext
                    )
                )
                cameraView.enableFpsMeter()
                cameraView.enableView()
            } else {
                Toast.makeText(applicationContext, "Could not load OpenCV", Toast.LENGTH_LONG).show()
                this@VideoStreamActivity.finish()
            }
        }

        override fun onPackageInstall(operation: Int, callback: InstallCallbackInterface?) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_stream)

        cameraView = findViewById(R.id.camera_view)
    }

    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, opencvLoaderCallback)
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!")
            opencvLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    override fun onPause() {
        super.onPause()
        cameraView.disableView()
    }
}

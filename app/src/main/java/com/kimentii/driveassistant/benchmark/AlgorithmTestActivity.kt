package com.kimentii.driveassistant.benchmark

import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.widget.ProgressBar
import com.kimentii.driveassistant.R
import com.kimentii.driveassistant.logger.log
import com.kimentii.driveassistant.views.DrawingView

class AlgorithmTestActivity : AppCompatActivity() {

    private lateinit var drawingView: DrawingView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_algorithm_test)

        drawingView = findViewById(R.id.drawing_view)
        progressBar = findViewById(R.id.progress_bar)

        val pathToVideo = PATH_TO_VIDEO_NEW_800_600_4_3
        log("Path to video: $pathToVideo")

        drawingView.setVideoPath(pathToVideo)

        val onVideoFrameReceivedListener = OnVideoFrameReceivedListener(this)
        drawingView.setOnProgressUpdatedListener { progress ->
            progressBar.progress = progress
            if (progress == MAX_PERCENT) {
                startResultActivityAndFinish(
                    pathToVideo,
                    onVideoFrameReceivedListener.detectedFramesCount
                )
            }
        }
        drawingView.setOnFrameReceived(onVideoFrameReceivedListener)
    }

    private fun startResultActivityAndFinish(pathToVideo: String, detectedFramesCount: Long) {
        val intent = Intent(this, BenchmarkResultActivity::class.java)

        intent.putExtras(BenchmarkResultActivity.getBundle(pathToVideo, detectedFramesCount))

        startActivity(intent)

        finish()
    }

    companion object {
        private const val MAX_PERCENT = 100
        private val PATH_TO_VIDEO_FOLDER = Environment.getExternalStorageDirectory().path + "/video/"
        private val PATH_TO_VIDEO_1280_720 = PATH_TO_VIDEO_FOLDER + "test.mp4"
        private val PATH_TO_VIDEO_800_600 = PATH_TO_VIDEO_FOLDER + "test_800_600.mp4"
        private val PATH_TO_VIDEO_NEW_800_600 = PATH_TO_VIDEO_FOLDER + "new_test_800_600.mp4"
        private val PATH_TO_VIDEO_NEW_800_600_4_3 = PATH_TO_VIDEO_FOLDER + "new_test_800_600_4_3.mp4"
        private val PATH_TO_VIDEO_SMALL_TEST_800_600 = PATH_TO_VIDEO_FOLDER + "small_test.mp4"
    }
}

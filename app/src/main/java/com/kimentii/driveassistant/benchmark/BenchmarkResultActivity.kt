package com.kimentii.driveassistant.benchmark

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.kimentii.driveassistant.R
import com.kimentii.driveassistant.video.VideoMetadata

class BenchmarkResultActivity : AppCompatActivity() {

    private lateinit var framesTotalTextView: TextView
    private lateinit var detectedFramesCountTextView: TextView
    private lateinit var recognitionPercentageTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_benchmark_result)

        framesTotalTextView = findViewById(R.id.tv_frames_total)
        detectedFramesCountTextView = findViewById(R.id.tv_detected_frames_count)
        recognitionPercentageTextView = findViewById(R.id.tv_recognition_percentage)

        val extras = intent.extras
        if (extras != null) {
            val pathToVideo = extras.getString(EXTRA_PATH_TO_VIDEO)!!
            val detectedFramesCount = extras.getLong(EXTRA_DETECTED_FRAMES_COUNT)
            val totalFramesCount = getTotalFramesCount(pathToVideo)
            val recognitionPercentage = (detectedFramesCount.toDouble() / totalFramesCount.toDouble()) * MAX_PERCENT

            framesTotalTextView.text = totalFramesCount.toString()
            detectedFramesCountTextView.text = detectedFramesCount.toString()
            recognitionPercentageTextView.text = recognitionPercentage.toString()
        }
    }

    private fun getTotalFramesCount(pathToVideo: String): Long {
        val videoMetadata = VideoMetadata()
        videoMetadata.loadMetadataFromSource(pathToVideo)

        return videoMetadata.getFrameCount()
    }

    companion object {
        private const val EXTRA_PATH_TO_VIDEO = "EXTRA_PATH_TO_VIDEO"
        private const val EXTRA_DETECTED_FRAMES_COUNT = "EXTRA_DETECTED_FRAMES_COUNT"
        private const val MAX_PERCENT = 100

        fun getBundle(pathToVideo: String, detectedFramesCount: Long): Bundle {
            return Bundle().apply {
                putString(EXTRA_PATH_TO_VIDEO, pathToVideo)
                putLong(EXTRA_DETECTED_FRAMES_COUNT, detectedFramesCount)
            }
        }
    }
}

package com.kimentii.driveassistant.benchmark

import android.content.Context
import com.kimentii.driveassistant.detectors.ProhibitorySignsDetector
import org.opencv.core.Mat
import org.opencv.core.Scalar
import org.opencv.imgproc.Imgproc

class OnVideoFrameReceivedListener(context: Context) : (Mat) -> Mat {

    private val prohibitorySignsDetector = ProhibitorySignsDetector(context)
    var detectedFramesCount = 0L
        private set

    override fun invoke(frame: Mat): Mat {
        val frameGrayscale = Mat()

        Imgproc.cvtColor(frame, frameGrayscale, Imgproc.COLOR_RGB2GRAY)

        val signs = prohibitorySignsDetector.detect(frameGrayscale).toList()
        if (signs.isNotEmpty()) {
            detectedFramesCount++
        }

        signs.forEach {
            Imgproc.rectangle(frame, it, Scalar(0.0, 255.0, 0.0, 255.0))
        }

        return frame
    }
}

package com.kimentii.driveassistant.classifier

import com.sh1r0.caffe_android_lib.CaffeMobile
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.CLAHE
import org.opencv.imgproc.Imgproc
import java.io.File

class Classifier(private val bufferImageFile: File) {

    companion object {
        private const val MODEL_PATH = "/sdcard/caffe_mobile/traffic-signs/config_deploy.prototxt"
        private const val WEIGHTS_PATH = "/sdcard/caffe_mobile/traffic-signs/_iter_10880.caffemodel"
        private const val SCALE = 0.00390625F
    }

    private val caffeMobile: CaffeMobile
    private val clache: CLAHE

    init {
        caffeMobile = CaffeMobile()
        caffeMobile.loadModel(MODEL_PATH, WEIGHTS_PATH)
        caffeMobile.setScale(SCALE)

        clache = Imgproc.createCLAHE()
    }

    fun classify(image: Mat): Sign {
        val resizedImage = Mat()
        Imgproc.resize(
            image,
            resizedImage,
            Size(32.toDouble(), 32.toDouble()),
            0.toDouble(),
            0.toDouble(),
            Imgproc.INTER_CUBIC
        )

        clache.apply(resizedImage, resizedImage)

        if (!bufferImageFile.exists()) {
            bufferImageFile.createNewFile()
        }

        Imgcodecs.imwrite(bufferImageFile.path, resizedImage)

        val predictResult = caffeMobile.predictImage(bufferImageFile.path)[0]

        val confidence = caffeMobile.getConfidenceScore(bufferImageFile.path)[predictResult]

        return if (confidence > 0.8f) {
            getSign(predictResult)
        } else {
            Sign.Unknown
        }
    }

    private fun getSign(signId: Int): Sign {
        return when (signId) {
            0 -> Sign.SpeedLimit(20)
            1 -> Sign.SpeedLimit(30)
            2 -> Sign.SpeedLimit(50)
            3 -> Sign.SpeedLimit(60)
            4 -> Sign.SpeedLimit(70)
            5 -> Sign.SpeedLimit(80)
            6 -> Sign.StandingProhibited
            7 -> Sign.SpeedLimit(100)
            8 -> Sign.SpeedLimit(120)
            9 -> Sign.OvertakingProhibited
            10 -> Sign.OvertakingByTrucksProhibited
            11 -> Sign.ParkingProhibited
            15 -> Sign.ClosedForVehicles
            16 -> Sign.NoEntryForTrucks
            17 -> Sign.NoEntry
            else -> Sign.Unknown
        }
    }
}

package com.kimentii.driveassistant.classifier

sealed class Sign(val name: String) {

    data class SpeedLimit(val limitKmH: Int) : Sign("Ограничение скорости") // 0,1,2,3,4,5,7,8
    object StandingProhibited : Sign("Остановка запрещена")  // 6
    object OvertakingProhibited : Sign("Обгон запрещен") // 9
    object OvertakingByTrucksProhibited : Sign("Обгон грузовым автомобилям запрещен") // 10
    object ParkingProhibited : Sign("Стоянка запрещена") // 11
    object ClosedForVehicles : Sign("Движение запрещено") //15
    object NoEntryForTrucks : Sign("Движение грузовых автомобилей запрещено") // 16
    object NoEntry : Sign("Въезд запрещен") // 17
    object Unknown : Sign("Unknown")
}

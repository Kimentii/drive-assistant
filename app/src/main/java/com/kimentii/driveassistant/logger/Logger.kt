package com.kimentii.driveassistant.logger

import android.util.Log

private const val TAG = "MyTag"

fun log(message: String) {
    Log.d(TAG, message)
}

package com.kimentii.driveassistant

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import com.kimentii.driveassistant.benchmark.AlgorithmTestActivity
import com.kimentii.driveassistant.streamingvideo.VideoStreamActivity
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.InstallCallbackInterface
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import kotlin.reflect.KClass

class MainActivity : AppCompatActivity() {

    private lateinit var algorithmParametersButton: Button
    private lateinit var videoStreamButton: Button
    private val opencvLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {}

        override fun onPackageInstall(operation: Int, callback: InstallCallbackInterface?) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        algorithmParametersButton = findViewById(R.id.button_algorithm_parameters)
        videoStreamButton = findViewById(R.id.button_video_stream)

        algorithmParametersButton.setOnClickListener { startNewActivity(AlgorithmTestActivity::class) }
        videoStreamButton.setOnClickListener { startNewActivity(VideoStreamActivity::class) }
    }

    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, opencvLoaderCallback)
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!")
            opencvLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    private fun <T : AppCompatActivity> startNewActivity(activityClass: KClass<T>) {
        val intent = Intent(this, activityClass.java)

        startActivity(intent)
    }

    companion object {
        private const val TAG = "MyTag"
    }

}

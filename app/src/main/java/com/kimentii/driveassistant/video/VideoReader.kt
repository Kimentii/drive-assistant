package com.kimentii.driveassistant.video

import com.kimentii.driveassistant.logger.log
import org.opencv.android.Utils
import org.opencv.core.Mat
import wseemann.media.FFmpegMediaMetadataRetriever
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class VideoReader(
    private val path: String,
    private val onFrameReceived: (Mat) -> Unit
) : Thread() {

    private val isReading = AtomicBoolean(true)
    private var onProgressUpdatedListener: ((Int) -> Unit)? = null

    init {
        start()
    }

    override fun run() {
        val mediaMetadataRetriever = FFmpegMediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(path)

        val metadata = mediaMetadataRetriever.metadata

        val framerate = metadata.getDouble(FFmpegMediaMetadataRetriever.METADATA_KEY_FRAMERATE)
        val durationMs = metadata.getLong(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION)

        log("Framerate: $framerate")
        log("durationMs: $durationMs")

        val timeBetweenFramesMs = ((1.0 / framerate) * TimeUnit.SECONDS.toMillis(1)).toLong()

        var currentTime = 0L
        while (isReading.get()) {
            val frame =
                mediaMetadataRetriever.getFrameAtTime(
                    TimeUnit.MILLISECONDS.toMicros(currentTime),
                    FFmpegMediaMetadataRetriever.OPTION_CLOSEST
                )
                    ?: break
            val frameAsMat = Mat()

            Utils.bitmapToMat(frame, frameAsMat)

            onFrameReceived(frameAsMat)

            onProgressUpdatedListener?.invoke((currentTime.toDouble() / durationMs.toDouble() * MAX_PERCENT).toInt())

            currentTime += timeBetweenFramesMs

            if (currentTime > durationMs) {
                break
            }
        }

        onProgressUpdatedListener?.invoke(MAX_PERCENT)

        log("Video finished")

        mediaMetadataRetriever.release()
    }

    fun setOnProgressUpdatedListener(listener: (Int) -> Unit) {
        onProgressUpdatedListener = listener
    }

    fun stopReading() {
        isReading.set(false)
    }

    companion object {
        private const val MAX_PERCENT = 100
    }
}

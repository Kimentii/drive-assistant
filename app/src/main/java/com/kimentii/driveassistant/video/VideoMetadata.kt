package com.kimentii.driveassistant.video

import wseemann.media.FFmpegMediaMetadataRetriever
import wseemann.media.FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION
import wseemann.media.FFmpegMediaMetadataRetriever.METADATA_KEY_FRAMERATE
import java.util.concurrent.TimeUnit

class VideoMetadata {

    private var metadata: HashMap<String, String>? = null

    fun loadMetadataFromSource(path: String) {
        val ffmpegMediaMetadataRetriever = FFmpegMediaMetadataRetriever()

        ffmpegMediaMetadataRetriever.setDataSource(path)

        metadata = ffmpegMediaMetadataRetriever.metadata.all

        ffmpegMediaMetadataRetriever.release()
    }

    fun getFrameCount(): Long {
        validateMetadata()

        val localMetadata = metadata!!

        val durationMs = localMetadata.getValue(METADATA_KEY_DURATION).toLong()
        val framerateFramesPerSec = localMetadata.getValue(METADATA_KEY_FRAMERATE).toDouble()

        val frameCount = TimeUnit.MILLISECONDS.toSeconds(durationMs) * framerateFramesPerSec

        return frameCount.toLong()
    }

    private fun validateMetadata() {
        if (metadata == null) {
            throw Exception("Call loadMetadataFromSource before extracting data.")
        }
    }
}

package com.kimentii.driveassistant.texttospeech

import android.content.Context
import android.speech.tts.TextToSpeech
import java.util.*

class TextPronouncer(context: Context) {

    private lateinit var textToSpeech: TextToSpeech
    private val onInitListenerImpl = TextToSpeech.OnInitListener {
        when (it) {
            TextToSpeech.SUCCESS -> {
                val locale = Locale("ru")

                val result = textToSpeech.setLanguage(locale)

                if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                    // TODO add notifying
                }
            }
        }
    }

    init {
        textToSpeech = TextToSpeech(context, onInitListenerImpl)
    }

    fun speak(text: String, queueMode: QueueMode) {
        textToSpeech.speak(text, queueMode.value, null, "ID")
    }

    fun shutdown() {
        textToSpeech.stop()
        textToSpeech.shutdown()
    }

    enum class QueueMode(val value: Int) {
        FLUSH(TextToSpeech.QUEUE_FLUSH),
        ADD(TextToSpeech.QUEUE_ADD)
    }
}

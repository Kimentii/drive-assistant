package com.kimentii.driveassistant

import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.runner.AndroidJUnit4
import com.kimentii.driveassistant.detectors.ProhibitorySignsDetector
import org.junit.Test
import org.junit.runner.RunWith
import org.opencv.imgcodecs.Imgcodecs
import java.io.File

@RunWith(AndroidJUnit4::class)
class DetectionTest {

    @Test
    fun testDetection() {
        System.loadLibrary("opencv_java4")

        val targetContext = getInstrumentation().targetContext

        val sourceFileDir = File("/sdcard/test_base")
        val allCategoryDirectories = sourceFileDir.listFiles().filter { it.isDirectory }
        val detector = ProhibitorySignsDetector(targetContext)
        val categoriesInfo = mutableListOf<CategoryInfo>()
        allCategoryDirectories.forEach { categoryDirectory ->
            val images = categoryDirectory.listFiles()
            var numberOfDetectedFiles = 0
            var detectTime = 0L
            images.forEach { imageFile ->
                val imageAsGray = Imgcodecs.imread(imageFile.path, Imgcodecs.IMREAD_GRAYSCALE)

                val startTime = System.currentTimeMillis()
                val detectedSigns = detector.detect(imageAsGray).toList()
                detectTime += (System.currentTimeMillis() - startTime)
                if (categoryDirectory.name != "no_sings") {
                    if (detectedSigns.size == 1) {
                        numberOfDetectedFiles++
                    }
                } else {
                    if (detectedSigns.size > 0) {
                        numberOfDetectedFiles++
                    }
                }
            }
            categoriesInfo.add(
                CategoryInfo(
                    categoryName = categoryDirectory.name,
                    totalFile = images.size,
                    detectedFiles = numberOfDetectedFiles,
                    detectionTime = detectTime
                )
            )
        }

        categoriesInfo.forEach {
            Log.d("MyTag", it.toString())
        }
        val categoriesWithoutNoSignCategory = categoriesInfo.filter { it.categoryName != "no_signs" }
        val totalNumberOfImagesWithSigns = categoriesWithoutNoSignCategory.sumBy { it.totalFile }
        val totalNumberOfDetectedSigns = categoriesWithoutNoSignCategory.sumBy { it.detectedFiles }
        val totalDetectTime = categoriesWithoutNoSignCategory.map { it.detectionTime }.sum()
        Log.d("MyTag", "Number of images with signs: $totalNumberOfImagesWithSigns")
        Log.d("MyTag", "Number of detected signs: $totalNumberOfDetectedSigns")
        Log.d("MyTag", "Medium detect time ${totalDetectTime.toDouble() / totalNumberOfImagesWithSigns}")
    }
}

data class CategoryInfo(
    val categoryName: String,
    val totalFile: Int,
    val detectedFiles: Int,
    val detectionTime: Long
)
